# Teplar Solutions Pvt Ltd
TEPLAR is a Business Intelligence and Analytics company that focus on using contemporary technology in Business Intelligence and Analytics, which will enable the business organization to reach the pinnacle in their performance, to stay ahead of its peers.

We transform data into knowledge to help organizations make better, more informed decisions.

Our team of experts work with Big Data, Big Data analytics, next generation dashboards, scorecards, key performance indicators, reports, data warehousing, and data integration.

## Contact US
Teplar Solutions Pvt Ltd

79, Ramanuja Nagar, Uppilipalayam, Coimbatore, Tamil Nadu, India � 641015.

Phone : +91 422 497 2112 / +91 89734 66515

Email : contact@teplar.com

Website : https://www.teplar.com/